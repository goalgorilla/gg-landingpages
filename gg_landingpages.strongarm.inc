<?php
/**
 * @file
 * gg_landingpages.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gg_landingpages_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_landingspagina';
  $strongarm->value = '0';
  $export['language_content_type_landingspagina'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_landingspagina';
  $strongarm->value = array();
  $export['menu_options_landingspagina'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_landingspagina';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_landingspagina'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_landingspagina';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_landingspagina'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_landingspagina';
  $strongarm->value = '0';
  $export['node_preview_landingspagina'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_landingspagina';
  $strongarm->value = 0;
  $export['node_submitted_landingspagina'] = $strongarm;

  return $export;
}
