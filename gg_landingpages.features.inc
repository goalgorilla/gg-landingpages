<?php
/**
 * @file
 * gg_landingpages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gg_landingpages_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function gg_landingpages_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function gg_landingpages_node_info() {
  $items = array(
    'landingspagina' => array(
      'name' => t('Landingspagina'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
